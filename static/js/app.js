'use strict';

var Piscine = angular.module('Piscine', ['ui','ui.bootstrap']);

Piscine.controller('MainCtrl', ['$http', '$scope', '$routeParams', '$window', function ($http, $scope, $routeParams, $window) {
    $http.get('/static/json/piscines.json').success(function (data, status) {
        $scope.piscines = data;
        $scope.selectPiscine = function (piscine) {
            $scope.piscine = piscine;
            showOnMap(piscine);
            scrollTo("map");
        };
    });
}]);

var mapInitialized = false;
var map;
var infowindow;
function initializeMap() {
    if (!mapInitialized) {
        var mapOptions = {
            center: new google.maps.LatLng(50.84556, 4.35694),
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

          // Try HTML5 geolocation
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = new google.maps.LatLng(position.coords.latitude,
                    position.coords.longitude);
                map.setCenter(pos);
                // do something with position
            }, function () {
                // Browser doesn't support Geolocation
            });
        } else {
            // Browser doesn't support Geolocation
        }
        mapInitialized = true;
    }
}

function showOnMap(piscine) {
    initializeMap();
    var pos = new google.maps.LatLng(piscine.geometry.coordinates[1], piscine.geometry.coordinates[0]);
    if (infowindow) infowindow.close();
    infowindow = new google.maps.InfoWindow({
        map: map,
        position: pos,
        content: piscine.properties.Name
    });


    map.setCenter(pos);
}

function scrollTo(hash) {
    window.location.hash = "/"; // to allow scrolling again later
    window.setTimeout(function () {
        window.location.hash = hash;
    }, 1);
}

window.setTimeout(initializeMap, 1);